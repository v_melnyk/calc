﻿Public Class Calculator
    Dim num1 As Double
    Dim special1 As String
    Dim num2 As Double
    Dim special2 As String
    Dim output As Double
    Dim Oper As String
    Dim iOperation As String
    Private Sub Calculator_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub button_click(sender As Object, e As EventArgs) Handles Button9.Click, Button7.Click, Button6.Click, Button5.Click, Button18.Click, Button15.Click, Button14.Click, Button13.Click, Button11.Click, Button10.Click
        Dim b As Button = sender
        If TextBox1.Text = "0" Then
            TextBox1.Text = b.Text
        Else
            TextBox1.Text = TextBox1.Text + b.Text
        End If
    End Sub

    Private Sub arithm_op(sender As Object, e As EventArgs) Handles Button8.Click, Button4.Click, Button16.Click, Button12.Click, Button3.Click, Button27.Click
        Dim op As Button = sender
        If TextBox1.Text.Count < 1 And op.Text = "-" Then
            TextBox1.Text = "-"
            Exit Sub
        ElseIf TextBox1.Text.Count < 1 Then
            special1 = "0"
            num1 = "0"
        Else
            special1 = TextBox1.Text
            num1 = TextBox1.Text
        End If
        TextBox2.Text = TextBox1.Text
        TextBox1.Text = ""
        Oper = op.Text
        If Oper = "x^" Then
            TextBox2.Text = TextBox2.Text + " ^"
        Else
            TextBox2.Text = TextBox2.Text + " " + Oper
        End If
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        If TextBox1.Text = "" Then
            special2 = "0"
            num2 = "0"
        Else
            special2 = TextBox1.Text
            num2 = TextBox1.Text
        End If
        If Oper = "+" Then

            If CheckBox1.Checked = True Then

                If special1.Contains("-") = True And special2.Contains("-") = False Then
                    special1 = Replace(special1, "-", "")
                    TextBox1.Text = big_sub(special2, special1, False)
                ElseIf special1.Contains("-") = False And special2.Contains("-") = True Then
                    special2 = Replace(special2, "-", "")
                    TextBox1.Text = big_sub(special1, special2, False)
                ElseIf special1.Contains("-") = True And special2.Contains("-") = True Then
                    special1 = Replace(special1, "-", "")
                    special2 = Replace(special2, "-", "")
                    TextBox1.Text = big_add(special1, special2, "-") 'StrReverse(out.Insert(decimal_place, "."))
                Else
                    TextBox1.Text = big_add(special1, special2, "-")
                End If

                TextBox2.Text = "" 'parts_1(0) + "." + parts_1(1) + " + " + parts_2(0) + "." + parts_2(1)

            Else

                output = num1 + num2
                TextBox1.Text = output
                TextBox2.Text = ""

            End If


        ElseIf Oper = "-" Then
            'output = num1 - num2

            Dim minus As String
            minus = ""
            If CheckBox1.Checked = True Then

                If special1.Contains("-") = True And special2.Contains("-") = False Then
                    special1 = Replace(special1, "-", "")
                    TextBox1.Text = big_add(special1, special2, "-")
                ElseIf special1.Contains("-") = False And special2.Contains("-") = True Then
                    special2 = Replace(special2, "-", "")
                    TextBox1.Text = big_add(special1, special2, "")
                ElseIf special1.Contains("-") = True And special2.Contains("-") = True Then
                    special1 = Replace(special1, "-", "")
                    special2 = Replace(special2, "-", "")
                    TextBox1.Text = big_sub(special1, special2, True)
                Else
                    TextBox1.Text = big_sub(special1, special2, False)
                End If

                TextBox2.Text = "" 'parts_1(0) + "." + parts_1(1) + " - " + parts_2(0) + "." + parts_2(1)
            Else
                output = num1 - num2
                TextBox1.Text = output
                TextBox2.Text = ""
            End If

        ElseIf Oper = "*" Then
            'output = num1 * num2

            Dim minus As String
                minus = ""
            If CheckBox1.Checked = True Then
                If special1.Contains("-") = True And special2.Contains("-") = False Then
                    special1 = Replace(special1, "-", "")
                    minus = "-"
                ElseIf special1.Contains("-") = False And special2.Contains("-") = True Then
                    special2 = Replace(special2, "-", "")
                    minus = "-"
                ElseIf special1.Contains("-") = True And special2.Contains("-") = True Then
                    special1 = Replace(special1, "-", "")
                    special2 = Replace(special2, "-", "")
                End If

                Dim parts_1 As String() = special1.Split(New Char() {"."c})
                Dim parts_2 As String() = special2.Split(New Char() {"."c})

                Dim dec_places_1 As Int32
                Dim dec_places_2 As Int32
                dec_places_1 = 0
                dec_places_2 = 0

                Dim it As Int32

                Dim first_number(parts_1(0).Count - 1) As Int32
                If parts_1.Count > 1 Then
                    dec_places_1 = parts_1(1).Count
                    it = 0
                    ReDim Preserve first_number(0 To Len((parts_1(0) + parts_1(1))) - 1)
                    For Each c As Char In (parts_1(0) + parts_1(1))
                        first_number(it) = Convert.ToInt32(c.ToString)
                        it += 1
                    Next
                Else
                    it = 0
                    For Each c As Char In parts_1(0)
                        first_number(it) = Convert.ToInt32(c.ToString)
                        it += 1
                    Next
                End If

                Dim second_number(parts_2(0).Count - 1) As Int32
                If parts_2.Count > 1 Then
                    dec_places_2 = parts_2(1).Count
                    it = 0
                    ReDim Preserve second_number(0 To Len((parts_2(0) + parts_2(1))) - 1)
                    For Each c As Char In (parts_2(0) + parts_2(1))
                        second_number(it) = Convert.ToInt32(c.ToString)
                        it += 1
                    Next
                Else
                    it = 0
                    For Each c As Char In parts_2(0)
                        second_number(it) = Convert.ToInt32(c.ToString)
                        it += 1
                    Next
                End If

                Dim decimal_place As Int64
                decimal_place = dec_places_1 + dec_places_2

                Dim flag As Boolean
                Dim moving As Int32
                Dim to_out As String
                Dim out As String
                Dim counter As Int32
                counter = 0
                to_out = "0"
                Dim debug1 As String
                For j As Integer = second_number.Count - 1 To 0 Step -1
                    out = ""
                    moving = 0
                    out += New String("0"c, counter)
                    For i As Integer = first_number.Count - 1 To 0 Step -1
                        Dim temp_o As Int32
                        temp_o = first_number(i) * second_number(j)
                        temp_o += moving
                        If temp_o >= 10 Then
                            moving = temp_o / 10
                            temp_o = temp_o Mod 10
                        Else
                            moving = 0
                        End If
                        out += temp_o.ToString

                        If out.Count > 250 Then
                            Exit For
                        End If
                    Next i
                    out = moving.ToString + StrReverse(out)

                    debug1 += out + " + "
                    to_out = big_add(to_out, out, "")
                    counter += 1
                Next j

                If decimal_place > 0 Then
                    to_out = to_out.Insert(to_out.Count - decimal_place, ".")
                End If

                If to_out(0) = "0" Then
                    to_out = to_out.Substring(1, out.Count - 1)
                End If

                TextBox1.Text = minus + to_out
                TextBox2.Text = "" 'String.Join(", ", first_number) + " * " + String.Join(", ", second_number) + " = " + debug1 'parts_1(0) + "." + parts_1(1) + " + " + parts_2(0) + "." + parts_2(1)
            Else
                output = num1 * num2
                TextBox1.Text = output
                TextBox2.Text = ""
            End If
        ElseIf Oper = "/" Then
            'output = num1 / num2
            If CheckBox1.Checked = True Then
                If special2.Replace("0", "").Replace("-", "").Replace(".", "").Count = 0 Then
                    TextBox2.Text = "ERROR 0 DIVISION!!!"
                    Exit Sub
                End If

                Dim minus As String
                minus = ""

                If special1.Contains("-") = True And special2.Contains("-") = False Then
                    special1 = Replace(special1, "-", "")
                    minus = "-"
                ElseIf special1.Contains("-") = False And special2.Contains("-") = True Then
                    special2 = Replace(special2, "-", "")
                    minus = "-"
                ElseIf special1.Contains("-") = True And special2.Contains("-") = True Then
                    special1 = Replace(special1, "-", "")
                    special2 = Replace(special2, "-", "")
                End If

                Dim parts_1 As String() = special1.Split(New Char() {"."c})
                Dim parts_2 As String() = special2.Split(New Char() {"."c})

                Dim add_zeros As Integer
                add_zeros = 0
                Dim dec_1 As Integer
                dec_1 = 0
                Dim dec_2 As Integer
                dec_2 = 0

                If parts_1.Count > 1 Then
                    dec_1 = parts_1(1).Count
                Else
                    ReDim Preserve parts_1(0 To UBound(parts_1) + 1)
                    parts_1(1) = ""
                End If
                If parts_2.Count > 1 Then
                    dec_2 = parts_2(1).Count
                Else
                    ReDim Preserve parts_2(0 To UBound(parts_2) + 1)
                    parts_2(1) = ""
                End If
                If dec_1 > dec_2 Then
                    parts_2(1) += New String("0", dec_1 - dec_2)
                ElseIf dec_2 > dec_1 Then
                    parts_1(1) += New String("0", dec_2 - dec_1)
                End If

                Dim first_number As String
                first_number = parts_1(0) + parts_1(1)
                Dim second_number As String
                second_number = parts_2(0) + parts_2(1)
                'TextBox1.Text = first_number + " :: " + second_number
                Dim full_part As String
                full_part = first_number.Replace("-", "")
                Dim full_part_number As Int64
                full_part_number = 0
                Dim full_part_r As String
                full_part_r = full_part

                While True
                    full_part = big_sub(full_part, second_number, False)
                    If full_part.Contains("-") = True Then
                        Exit While
                    End If
                    full_part_number += 1
                    full_part_r = full_part
                    'TextBox1.Text = full_part_number
                End While

                Dim dec_part As String
                Dim dec_part_current As String
                dec_part = full_part_r.Replace("-", "") + "0"
                Dim dec_part_number As Int64
                'Dim counter As Int64
                'counter = 0
                Dim dec_i As String
                dec_i = ""

                While True
                    dec_part_number = 0
                    While True
                        dec_part_current = dec_part
                        dec_part = big_sub(dec_part, second_number, False)
                        If dec_part.Contains("-") = True Then
                            Exit While
                        End If
                        dec_part_number += 1
                        'TextBox1.Text = dec_part_number
                    End While
                    'counter += 1
                    dec_part = dec_part_current + "0"
                    'dec_part = dec_part.Replace("-", "")
                    If dec_part.Replace("0", "").Count = 0 Or dec_i.Count > 100 Then
                        dec_i += dec_part_number.ToString
                        Exit While
                    End If
                    dec_i += dec_part_number.ToString
                End While

                TextBox1.Text = minus + full_part_number.ToString + "." + dec_i
                TextBox2.Text = ""
            Else
                output = num1 / num2
                TextBox1.Text = output
                TextBox2.Text = ""
            End If
        ElseIf Oper = "%" Then
                output = num1 Mod num2
                'TextBox1.Text = String.Format("{0:N20}", output)
                TextBox1.Text = output
                TextBox2.Text = ""
            ElseIf Oper = "x^" Then
                output = num1 ^ num2
            'TextBox1.Text = String.Format("{0:N20}", output)
            TextBox1.Text = output
            TextBox2.Text = ""
        End If
    End Sub

    Private Sub dot_click(sender As Object, e As EventArgs) Handles Button17.Click
        If InStr(TextBox1.Text, ".") = 0 Then
            TextBox1.Text = TextBox1.Text + "."
        End If
    End Sub

    Private Sub clear_click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        TextBox1.Text = "3.14159265358979323846264338327950288419716939937510"
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        TextBox1.Text = "2.71828182845904523536028747135266249775724709369995"
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Dim log_ As Double
        log_ = Double.Parse(TextBox1.Text)
        TextBox2.Text = System.Convert.ToString("log(" + (TextBox1.Text) + ")")
        log_ = System.Math.Log10(log_)
        'TextBox1.Text = String.Format("{0:N20}", log_)
        TextBox1.Text = log_
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        Dim sqrt_ As Double
        sqrt_ = Double.Parse(TextBox1.Text)
        TextBox2.Text = System.Convert.ToString("sqrt(" + (TextBox1.Text) + ")")
        sqrt_ = System.Math.Sqrt(sqrt_)
        'TextBox1.Text = String.Format("{0:N20}", sqrt_)
        TextBox1.Text = sqrt_
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        Dim sin_ As Double
        sin_ = Double.Parse(TextBox1.Text)
        TextBox2.Text = System.Convert.ToString("sin(" + (TextBox1.Text) + ")")
        sin_ = System.Math.Sin(sin_)
        'TextBox1.Text = String.Format("{0:N20}", sin_)
        TextBox1.Text = sin_
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        Dim cos_ As Double
        cos_ = Double.Parse(TextBox1.Text)
        TextBox2.Text = System.Convert.ToString("sin(" + (TextBox1.Text) + ")")
        cos_ = System.Math.Cos(cos_)
        'TextBox1.Text = String.Format("{0:N20}", cos_)
        TextBox1.Text = cos_
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click
        Dim tan_ As Double
        tan_ = Double.Parse(TextBox1.Text)
        TextBox2.Text = System.Convert.ToString("sin(" + (TextBox1.Text) + ")")
        tan_ = System.Math.Tan(tan_)
        'TextBox1.Text = String.Format("{0:N20}", tan_)
        TextBox1.Text = tan_
    End Sub

    Function big_sub(ByVal fst As String, ByVal snd As String, ByVal minus As Boolean) As String
        Dim parts_1 As String() = fst.Split(New Char() {"."c})
        Dim parts_2 As String() = snd.Split(New Char() {"."c})

        If UBound(parts_1) = 0 Then
            ReDim Preserve parts_1(0 To UBound(parts_1) + 1)
            parts_1(1) = "0"
        End If

        If UBound(parts_2) = 0 Then
            ReDim Preserve parts_2(0 To UBound(parts_2) + 1)
            parts_2(1) = "0"
        End If

        If Len(parts_1(1)) > Len(parts_2(1)) Then
            While Len(parts_1(1)) > Len(parts_2(1))
                parts_2(1) += "0"
            End While
        ElseIf Len(parts_2(1)) > Len(parts_1(1)) Then
            While Len(parts_2(1)) > Len(parts_1(1))
                parts_1(1) += "0"
            End While
        End If

        If Len(parts_1(0)) > Len(parts_2(0)) Then
            Dim tmp1 As String
            While Len(parts_1(0)) > Len(parts_2(0))
                tmp1 = parts_2(0)
                parts_2(0) = "0" + tmp1
            End While
        ElseIf Len(parts_2(0)) > Len(parts_1(0)) Then
            Dim tmp2 As String
            While Len(parts_2(0)) > Len(parts_1(0))
                tmp2 = parts_1(0)
                parts_1(0) = "0" + tmp2
            End While
        End If

        Dim decimal_place As Int64
        decimal_place = parts_2(1).Count + 1

        Dim first_number(Len((parts_1(0) + parts_1(1)))) As Int32
        Dim it As Int32
        it = 0
        For Each c As Char In (parts_1(0) + parts_1(1))
            first_number(it) = Convert.ToInt32(c.ToString)
            it += 1

        Next

        Dim second_number(Len((parts_2(0) + parts_2(1)))) As Int32
        it = 0
        For Each c As Char In (parts_2(0) + parts_2(1))
            second_number(it) = Convert.ToInt32(c.ToString)
            it += 1
        Next

        Dim minus_flag As Boolean
        minus_flag = False
        For i As Integer = 0 To second_number.Count - 1 Step 1
            If first_number(i) > second_number(i) Then
                minus_flag = False
                Exit For
            ElseIf first_number(i) < second_number(i) Then
                minus_flag = True
                Exit For
            End If
        Next

        Dim flag As Boolean
        Dim out As String
        out = ""
        flag = False
        For i As Integer = second_number.Count - 1 To 0 Step -1
            Dim temp_o As Int32
            If minus_flag = False Then
                temp_o = first_number(i) - second_number(i)
            ElseIf minus_flag = True Then
                temp_o = second_number(i) - first_number(i)
            End If
            If flag = True Then
                temp_o -= 1
            End If
            flag = False
            If temp_o < 0 Then
                temp_o += 10
                flag = True
            End If
            out += temp_o.ToString
            If out.Count > 250 Then
                Exit For
            End If
        Next i
        If minus_flag = True Then
            out += "-"
        End If

        out = StrReverse(out.Insert(decimal_place, "."))
        If out.Contains(".") Then
            If out(out.Count - 1) = "0" Then
                out = out.Substring(0, out.Count - 1)
            End If
            If out(out.Count - 1) = "0" And out(out.Count - 2) = "." Then
                out = out.Substring(0, out.Count - 2)
            End If
        End If

        If out.Count > 0 Then
            While out.Count > 0
                If out(0) = "0" Then
                    out = out.Substring(1, out.Count - 1)
                Else
                    Exit While
                End If
            End While
        End If

        If out.Contains("-") = True And minus = True Then
            Return out.Replace("-", "") '(out(out.Count - 1) = "0").ToString
        ElseIf out.Contains("-") = False And minus = True Then
            Return out = "-" + out
        Else
            Return out
        End If
    End Function

    Function big_add(ByVal fst As String, ByVal snd As String, ByRef minus As String) As String
        Dim parts_1 As String() = fst.Split(New Char() {"."c})
        Dim parts_2 As String() = snd.Split(New Char() {"."c})

        If parts_1.Count = 1 Then
            ReDim Preserve parts_1(0 To UBound(parts_1) + 1)
            parts_1(1) = "0"
        End If

        If parts_2.Count = 1 Then
            ReDim Preserve parts_2(0 To UBound(parts_2) + 1)
            parts_2(1) = "0"
        End If

        If Len(parts_1(1)) > Len(parts_2(1)) Then
            While Len(parts_1(1)) > Len(parts_2(1))
                parts_2(1) += "0"
            End While
        ElseIf Len(parts_2(1)) > Len(parts_1(1)) Then
            While Len(parts_2(1)) > Len(parts_1(1))
                parts_1(1) += "0"
            End While
        End If

        If Len(parts_1(0)) > Len(parts_2(0)) Then
            Dim tmp1 As String
            While Len(parts_1(0)) > Len(parts_2(0))
                tmp1 = parts_2(0)
                parts_2(0) = "0" + tmp1
            End While
        ElseIf Len(parts_2(0)) > Len(parts_1(0)) Then
            Dim tmp2 As String
            While Len(parts_2(0)) > Len(parts_1(0))
                tmp2 = parts_1(0)
                parts_1(0) = "0" + tmp2
            End While
        End If

        Dim decimal_place As Int64
        decimal_place = parts_2(1).Count + 1

        Dim first_number(Len((parts_1(0) + parts_1(1)))) As Int32
        Dim it As Int32
        it = 0
        For Each c As Char In (parts_1(0) + parts_1(1))
            first_number(it) = Convert.ToInt32(c.ToString)
            it += 1
        Next

        Dim second_number(Len((parts_2(0) + parts_2(1)))) As Int32
        it = 0
        For Each c As Char In (parts_2(0) + parts_2(1))
            second_number(it) = Convert.ToInt32(c.ToString)
            it += 1
        Next

        Dim flag As Boolean
        Dim out As String
        out = ""
        flag = False
        For i As Integer = second_number.Count - 1 To 0 Step -1
            Dim temp_o As Int32
            temp_o = first_number(i) + second_number(i)
            If flag = True Then
                temp_o += 1
            End If
            flag = False
            If temp_o >= 10 Then
                temp_o -= 10
                flag = True
            End If
            out += temp_o.ToString

            If out.Count > 250 Then
                Exit For
            End If
        Next i
        If flag = True Then
            out += "1"
        End If

        out = StrReverse(out.Insert(decimal_place, "."))
        If out.Contains(".") Then
            If out(out.Count - 1) = "0" Then
                out = out.Substring(0, out.Count - 1)
            End If
            If out(out.Count - 1) = "0" And out(out.Count - 2) = "." Then
                out = out.Substring(0, out.Count - 2)
            End If
        End If

        Return minus + out '(out(out.Count - 1) = "0").ToString
    End Function

End Class
