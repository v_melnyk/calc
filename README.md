# Calculator

As specified in web example, calculator works with full precision with +, -, *, / functions.

They are max 200 characters long, yet still with big heavy calculations operations (esspecially /) can be really really slow.

Other operations are done on regular floating point operations, since even the example doesn't give them as FP.